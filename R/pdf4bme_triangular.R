
#### name:tri ####
# Imagine a point where the value of the soft data is 4.3431

# and you want to give it a soft "range" of 1.8 aorund that
#x = 4.3431
#r = 1.8
#xin <- sft$residls[1:10]
#rin <- rep(.6, 10)

pdf4bme_triangular <- function(x  = xin[1:4], r = rin[1:4]){
  x
  st = x-r/2
  st
  ed = x+r/2
  xs = data.frame(st,x,ed)
  xs
  ys = data.frame(v1=rep(0, nrow(xs)),
    v2=rep(2, nrow(xs)),
    v3=rep(0, nrow(xs))
    )
  ys
  ar = 0.5 * (xs$ed-xs$st) * ys[,2]
  ar
  ys = ys/ar  
  output <- data.frame(3,xs,ys)
  return(output)
}

#xdat <- cbind(xin, rin)
#thing <- pdf4bme_triangular(xin, rin)
#thing
